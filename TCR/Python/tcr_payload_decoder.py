"""
  TCR Payload Decoder

  THIS SOFTWARE IS PROVIDED BY PARAMETRIC GMBH AND ITS CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from math import floor


# VERSION 1

def app_payload_v1_decoder(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'SBX_BATT': '', 'SBX_PV': '', 'TEMP': '', 'L0_CNT': '', 'L0_AVG': '', 'R0_CNT': '', 'R0_AVG': '',
                       'L1_CNT': '', 'L1_AVG': '', 'R1_CNT': '',
                       'R1_AVG': '', 'L2_CNT': '', 'L2_AVG': '', 'R2_CNT': '', 'R2_AVG': '', 'L3_CNT': '', 'L3_AVG': '',
                       'R3_CNT': '', 'R3_AVG': ''}

    if port != 15:
        print('ERROR: Wrong port! TCR devices are using port 15 for application payloads.')
        return None

    if len(bytes_) != 32:
        print('ERROR: Wrong payload length')
        return None

    # Check for Parametric TCR v1 payload
    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x01:

        decoded_payload['SBX_BATT'] = bytes_[3]
        decoded_payload['SBX_PV'] = (bytes_[4] << 8 | bytes_[5])
        temp = (bytes_[6] << 8) | (bytes_[7])
        decoded_payload['TEMP'] = floor(bin16dec(temp) / 10)

        # Speed class 1
        decoded_payload['L0_CNT'] = (bytes_[8] << 8 | bytes_[9])
        decoded_payload['L0_AVG'] = bytes_[10]
        decoded_payload['R0_CNT'] = (bytes_[11] << 8 | bytes_[12])
        decoded_payload['R0_AVG'] = bytes_[13]

        # Speed class 2
        decoded_payload['L1_CNT'] = (bytes_[14] << 8 | bytes_[15])
        decoded_payload['L1_AVG'] = bytes_[16]
        decoded_payload['R1_CNT'] = (bytes_[17] << 8 | bytes_[18])
        decoded_payload['R1_AVG'] = bytes_[19]

        # Speed class 3
        decoded_payload['L2_CNT'] = (bytes_[20] << 8 | bytes_[21])
        decoded_payload['L2_AVG'] = bytes_[22]
        decoded_payload['R2_CNT'] = (bytes_[23] << 8 | bytes_[24])
        decoded_payload['R2_AVG'] = bytes_[25]

        # Speed class 4
        decoded_payload['L3_CNT'] = (bytes_[26] << 8 | bytes_[27])
        decoded_payload['L3_AVG'] = bytes_[28]
        decoded_payload['R3_CNT'] = (bytes_[29] << 8 | bytes_[30])
        decoded_payload['R3_AVG'] = bytes_[31]

    else:
        print('ERROR: TCR application payload V1 should start with be0201..')

    return decoded_payload


def config_payload_v1_decoder(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'FirmwareVersion': '', 'OperatingMode': '', 'DeviceClass': '',
                       'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '', 'HoldoffTime': '',
                       'RadarSensitivity': '', 'LTRLaneDist': '', 'RTLLaneDist': '', 'SC0_START': '', 'SC0_END': '',
                       'SC1_START': '', 'SC1_END': '', 'SC2_START': '', 'SC2_END': '', 'SC3_START': '', 'SC3_END': ''}

    if port != 190:
        print('ERROR: Wrong port! TCR devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 27:
        print('ERROR: Wrong payload length')
        return None

    # Check for Parametric TCR v1 payload
    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x01:

        decoded_payload['DeviceType'] = bytes_[3]
        decoded_payload['FirmwareVersion'] = str(bytes_[4]) + '.' + str(bytes_[5]) + '.' + str(bytes_[6])
        decoded_payload['OperatingMode'] = bytes_[7]
        decoded_payload['DeviceClass'] = bytes_[8]
        decoded_payload['UplinkType'] = bytes_[9]
        decoded_payload['UplinkInterval'] = (bytes_[10] << 8 | bytes_[11])
        decoded_payload['LinkCheckInterval'] = (bytes_[12] << 8 | bytes_[13])
        decoded_payload['HoldoffTime'] = (bytes_[14] << 8 | bytes_[15])
        decoded_payload['RadarSensitivity'] = bytes_[16]
        decoded_payload['LTRLaneDist'] = bytes_[17]
        decoded_payload['RTLLaneDist'] = bytes_[18]
        decoded_payload['SC0_START'] = bytes_[19]
        decoded_payload['SC0_END'] = bytes_[20]
        decoded_payload['SC1_START'] = bytes_[21]
        decoded_payload['SC1_END'] = bytes_[22]
        decoded_payload['SC2_START'] = bytes_[23]
        decoded_payload['SC2_END'] = bytes_[24]
        decoded_payload['SC3_START'] = bytes_[25]
        decoded_payload['SC3_END'] = bytes_[26]

    else:
        print('ERROR: TCR configuration payload V1 should start with be0201..')

    return decoded_payload


# VERSION 2

def app_payload_v2_decoder(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'SBX_BATT': '', 'SBX_PV': '', 'TEMP': '', 'L0_CNT': '', 'L0_AVG': '', 'R0_CNT': '', 'R0_AVG': '',
                       'L1_CNT': '', 'L1_AVG': '', 'R1_CNT': '',
                       'R1_AVG': '', 'L2_CNT': '', 'L2_AVG': '', 'R2_CNT': '', 'R2_AVG': '', 'L3_CNT': '', 'L3_AVG': '',
                       'R3_CNT': '', 'R3_AVG': ''}

    if port != 15:
        print('ERROR: Wrong port! TCR devices are using port 15 for application payloads.')
        return None

    if len(bytes_) != 33:
        print('ERROR: Wrong payload length')
        return None

    # Check for Parametric TCR v2 payload
    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x02:

        decoded_payload['SBX_BATT'] = (bytes_[3] << 8 | bytes_[4])
        decoded_payload['SBX_PV'] = (bytes_[5] << 8 | bytes_[6])
        temp = (bytes_[7] << 8) | (bytes_[8])
        decoded_payload['TEMP'] = floor(bin16dec(temp) / 10)

        # Speed class 1
        decoded_payload['L0_CNT'] = (bytes_[9] << 8 | bytes_[10])
        decoded_payload['L0_AVG'] = bytes_[11]
        decoded_payload['R0_CNT'] = (bytes_[12] << 8 | bytes_[13])
        decoded_payload['R0_AVG'] = bytes_[14]

        # Speed class 2
        decoded_payload['L1_CNT'] = (bytes_[15] << 8 | bytes_[16])
        decoded_payload['L1_AVG'] = bytes_[17]
        decoded_payload['R1_CNT'] = (bytes_[18] << 8 | bytes_[19])
        decoded_payload['R1_AVG'] = bytes_[20]

        # Speed class 3
        decoded_payload['L2_CNT'] = (bytes_[21] << 8 | bytes_[22])
        decoded_payload['L2_AVG'] = bytes_[23]
        decoded_payload['R2_CNT'] = (bytes_[24] << 8 | bytes_[25])
        decoded_payload['R2_AVG'] = bytes_[26]

        # Speed class 4
        decoded_payload['L3_CNT'] = (bytes_[27] << 8 | bytes_[28])
        decoded_payload['L3_AVG'] = bytes_[29]
        decoded_payload['R3_CNT'] = (bytes_[30] << 8 | bytes_[31])
        decoded_payload['R3_AVG'] = bytes_[32]

    else:
        print('ERROR: TCR application payload V2 should start with be0202..')

    return decoded_payload


def config_payload_v2_decoder(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'FirmwareVersion': '', 'OperatingMode': '', 'DeviceClass': '',
                       'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '', 'HoldoffTime': '',
                       'RadarSensitivity': '', 'LTRLaneDist': '', 'RTLLaneDist': '', 'SC0_START': '', 'SC0_END': '',
                       'SC1_START': '', 'SC1_END': '', 'SC2_START': '', 'SC2_END': '', 'SC3_START': '', 'SC3_END': ''}

    if port != 190:
        print('ERROR: Wrong port! TCR devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 29:
        print('ERROR: Wrong payload length')
        return None

    # Check for Parametric TCR v2 payload
    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x02:

        decoded_payload['DeviceType'] = bytes_[3]
        decoded_payload['FirmwareVersion'] = str(bytes_[4]) + '.' + str(bytes_[5]) + '.' + str(bytes_[6])
        decoded_payload['OperatingMode'] = bytes_[7]
        decoded_payload['DeviceClass'] = bytes_[8]
        decoded_payload['UplinkType'] = bytes_[9]
        decoded_payload['UplinkInterval'] = (bytes_[10] << 8 | bytes_[11])
        decoded_payload['LinkCheckInterval'] = (bytes_[12] << 8 | bytes_[13])
        decoded_payload['HoldoffTime'] = (bytes_[14] << 8 | bytes_[15])
        decoded_payload['RadarSensitivity'] = bytes_[16]
        decoded_payload['LTRLaneDist'] = (bytes_[17] << 8 | bytes_[18])
        decoded_payload['RTLLaneDist'] = (bytes_[19] << 8 | bytes_[20])
        decoded_payload['SC0_START'] = bytes_[21]
        decoded_payload['SC0_END'] = bytes_[22]
        decoded_payload['SC1_START'] = bytes_[23]
        decoded_payload['SC1_END'] = bytes_[24]
        decoded_payload['SC2_START'] = bytes_[25]
        decoded_payload['SC2_END'] = bytes_[26]
        decoded_payload['SC3_START'] = bytes_[27]
        decoded_payload['SC3_END'] = bytes_[28]

    else:
        print('ERROR: TCR configuration payload V2 should start with be0202..')

    return decoded_payload


# VERSION 3

def config_payload_v3_decoder(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'FirmwareVersion': '', 'OperatingMode': '', 'DeviceClass': '',
                       'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '', 'HoldoffTime': '',
                       'RadarAutotuning': '', 'RadarSensitivity': '', 'LTRLaneDist': '', 'RTLLaneDist': '',
                       'SC0_START': '', 'SC0_END': '', 'SC1_START': '', 'SC1_END': '', 'SC2_START': '', 'SC2_END': '',
                       'SC3_START': '', 'SC3_END': '', 'SBX_Firmware': ''}

    if port != 190:
        print('ERROR: Wrong port! TCR devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 33:
        print('ERROR: Wrong payload length')
        return None

    # Check for Parametric TCR v3 payload
    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x03:

        decoded_payload['DeviceType'] = bytes_[3]
        decoded_payload['FirmwareVersion'] = str(bytes_[4]) + '.' + str(bytes_[5]) + '.' + str(bytes_[6])
        decoded_payload['OperatingMode'] = bytes_[7]
        decoded_payload['DeviceClass'] = bytes_[8]
        decoded_payload['UplinkType'] = bytes_[9]
        decoded_payload['UplinkInterval'] = (bytes_[10] << 8 | bytes_[11])
        decoded_payload['LinkCheckInterval'] = (bytes_[12] << 8 | bytes_[13])
        decoded_payload['HoldoffTime'] = (bytes_[14] << 8 | bytes_[15])
        decoded_payload['RadarAutotuning'] = bytes_[16]
        decoded_payload['RadarSensitivity'] = bytes_[17]
        decoded_payload['LTRLaneDist'] = (bytes_[18] << 8 | bytes_[19])
        decoded_payload['RTLLaneDist'] = (bytes_[20] << 8 | bytes_[21])
        decoded_payload['SC0_START'] = bytes_[22]
        decoded_payload['SC0_END'] = bytes_[23]
        decoded_payload['SC1_START'] = bytes_[24]
        decoded_payload['SC1_END'] = bytes_[25]
        decoded_payload['SC2_START'] = bytes_[26]
        decoded_payload['SC2_END'] = bytes_[27]
        decoded_payload['SC3_START'] = bytes_[28]
        decoded_payload['SC3_END'] = bytes_[29]
        decoded_payload['SBX_Firmware'] = str(bytes_[30]) + '.' + str(bytes_[31]) + '.' + str(bytes_[32])

    else:
        print('ERROR: TCR configuration payload V3 should start with be0203..')

    return decoded_payload


def bin16dec(bin_):
    num = bin_ & 0xFFFF
    if 0x8000 & num:
        num = -(0x010000 - num)
    return num


' Generic Decoder '


def decode(payload, port):
    bytes_ = bytearray.fromhex(payload)

    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x01:
        if port == 15:
            return app_payload_v1_decoder(payload, port)
        if port == 190:
            return config_payload_v1_decoder(payload, port)

    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x02:
        if port == 15:
            return app_payload_v2_decoder(payload, port)
        if port == 190:
            return config_payload_v2_decoder(payload, port)

    if bytes_[0] == 0xbe and bytes_[1] == 0x02 and bytes_[2] == 0x03:
        if port == 190:
            return config_payload_v3_decoder(payload, port)
