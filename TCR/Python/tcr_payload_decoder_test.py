import unittest
from tcr_payload_decoder import decode


class Test_TCR_Payload_Decoder(unittest.TestCase):
    # Test application payload v1 decoder
    def test_app_payload_v1_decoder(self):
        decoded_payload = decode('be02016412c218b800000000010600000000020b00000000011e000000000000', 15)
        self.assertEqual(decoded_payload,
                         {'SBX_BATT': 100, 'SBX_PV': 4802, 'TEMP': 632, 'L0_CNT': 0, 'L0_AVG': 0, 'R0_CNT': 1,
                          'R0_AVG': 6, 'L1_CNT': 0, 'L1_AVG': 0, 'R1_CNT': 2, 'R1_AVG': 11, 'L2_CNT': 0, 'L2_AVG': 0,
                          'R2_CNT': 1, 'R2_AVG': 30, 'L3_CNT': 0, 'L3_AVG': 0, 'R3_CNT': 0, 'R3_AVG': 0})

    # Test configuration payload v1 decoder
    def test_config_payload_v1_decoder(self):
        decoded_payload = decode('be020100010000000000000305a0000064050f010708191a313278', 190)
        self.assertEqual(decoded_payload,
                         {'DeviceType': 0, 'FirmwareVersion': '1.0.0', 'OperatingMode': 0, 'DeviceClass': 0,
                          'UplinkType': 0, 'UplinkInterval': 3, 'LinkCheckInterval': 1440, 'HoldoffTime': 0,
                          'RadarSensitivity': 100, 'LTRLaneDist': 5, 'RTLLaneDist': 15, 'SC0_START': 1, 'SC0_END': 7,
                          'SC1_START': 8, 'SC1_END': 25, 'SC2_START': 26, 'SC2_END': 49, 'SC3_START': 50,
                          'SC3_END': 120})

    # Test application payload v2 decoder
    def test_app_payload_v2_decoder(self):
        decoded_payload = decode('be0202001200000000000003000000000000000000000000000000000000000000', 15)
        self.assertEqual(decoded_payload,
                         {'SBX_BATT': 18, 'SBX_PV': 0, 'TEMP': 0, 'L0_CNT': 0, 'L0_AVG': 3, 'R0_CNT': 0, 'R0_AVG': 0,
                          'L1_CNT': 0, 'L1_AVG': 0, 'R1_CNT': 0, 'R1_AVG': 0, 'L2_CNT': 0, 'L2_AVG': 0, 'R2_CNT': 0,
                          'R2_AVG': 0, 'L3_CNT': 0, 'L3_AVG': 0, 'R3_CNT': 0, 'R3_AVG': 0})

    # Test configuration payload v2 decoder
    def test_config_payload_v2_decoder(self):
        decoded_payload = decode('be020201010101000001000a05a000006400fa00fa0107082800000000', 190)
        self.assertEqual(decoded_payload,
                         {'DeviceType': 1, 'FirmwareVersion': '1.1.1', 'OperatingMode': 0, 'DeviceClass': 0,
                          'UplinkType': 1, 'UplinkInterval': 10, 'LinkCheckInterval': 1440, 'HoldoffTime': 0,
                          'RadarSensitivity': 100, 'LTRLaneDist': 250, 'RTLLaneDist': 250, 'SC0_START': 1, 'SC0_END': 7,
                          'SC1_START': 8, 'SC1_END': 40, 'SC2_START': 0, 'SC2_END': 0, 'SC3_START': 0, 'SC3_END': 0})

    # Test configuration payload v3 decoder
    def test_config_payload_v3_decoder(self):
        decoded_payload = decode('be020300010300000001000a05a00000005a00fa00fa0107082800000000040100', 190)
        self.assertEqual(decoded_payload,
                         {'DeviceType': 0, 'FirmwareVersion': '1.3.0', 'OperatingMode': 0, 'DeviceClass': 0,
                          'UplinkType': 1, 'UplinkInterval': 10, 'LinkCheckInterval': 1440, 'HoldoffTime': 0,
                          'RadarAutotuning': 0, 'RadarSensitivity': 90, 'LTRLaneDist': 250, 'RTLLaneDist': 250,
                          'SC0_START': 1, 'SC0_END': 7, 'SC1_START': 8, 'SC1_END': 40, 'SC2_START': 0,
                          'SC2_END': 0, 'SC3_START': 0, 'SC3_END': 0, 'SBX_Firmware': '4.1.0'})


if __name__ == '__main__':
    unittest.main()
