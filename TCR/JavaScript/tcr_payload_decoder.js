/**
 * TCR Payload Decoders
 *
 * THIS SOFTWARE IS PROVIDED BY PARAMETRIC GMBH AND ITS CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
 * OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 *   
*/

/**
 * Application payload decoder v1
 * @param {byteArray} bytes 
 * @param {int} port 
 */
function app_payload_v1_decoder(bytes, port) {
    var obj = {};

    if (port != 15) {
        console.log("ERROR: Wrong port! TCR devices are using port 15 for application payloads.");
        return obj;
    }

    if (bytes.length != 32) {
        console.log("ERROR: Wrong payload length");
        return obj;
    }

    // check for Parametric TCR V1 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x01) {

        obj.SBX_BATT = bytes[3];                        // battery gauge when equiped with an SBX solar charger 0…100%
        obj.SBX_PV = (bytes[4] << 8) | (bytes[5]);      // Solar panel power when equiped with SBX 0…65535 mW

        var temp = (bytes[6] << 8) | (bytes[7]);
        temp = bin16dec(temp);
        obj.TEMP = Math.floor(temp / 10);               // CPU Temperature

        obj.L0_CNT = (bytes[8] << 8) | (bytes[9]);      // object count from left in speed class 0, 0-65535
        obj.L0_AVG = bytes[10];                         // average speed from left in speed class 0, 0-65535
        obj.R0_CNT = (bytes[11] << 8) | (bytes[12]);    // object count from right in speed class 0, 0-65535
        obj.R0_AVG = bytes[13];                         // average speed from right in speed class 0, 0-65535

        obj.L1_CNT = (bytes[14] << 8) | (bytes[15]);    // object count from left in speed class 1, 0-65535
        obj.L1_AVG = bytes[16];                         // average speed from left in speed class 1, 0-65535
        obj.R1_CNT = (bytes[17] << 8) | (bytes[18]);    // object count from right in speed class 1, 0-65535
        obj.R1_AVG = bytes[19];                         // average speed from right in speed class 1, 0-65535

        obj.L2_CNT = (bytes[20] << 8) | (bytes[21]);    // object count from left in speed class 1, 0-65535
        obj.L2_AVG = bytes[22];                         // average speed from left in speed class 1, 0-65535
        obj.R2_CNT = (bytes[23] << 8) | (bytes[24]);    // object count from right in speed class 1, 0-65535
        obj.R2_AVG = bytes[25];                         // average speed from right in speed class 1, 0-65535

        obj.L3_CNT = (bytes[26] << 8) | (bytes[27]);    // object count from left in speed class 1, 0-65535
        obj.L3_AVG = bytes[28];                         // average speed from left in speed class 1, 0-65535
        obj.R3_CNT = (bytes[29] << 8) | (bytes[30]);    // object count from right in speed class 1, 0-65535
        obj.R3_AVG = bytes[31];                         // average speed from right in speed class 1, 0-65535
    }
    else {
        console.log("ERROR: TCR application payload V1 should start with be0201..  ");
    }
    return obj;
}

/**
 * Application payload decoder v2
 * @param {byteArray} bytes 
 * @param {int} port 
 */
function app_payload_v2_decoder(bytes, port) {
    var obj = {};

    if (port != 15) {
        console.log("ERROR: Wrong port! TCR devices are using port 15 for application payloads.");
        return obj;
    }

    if (bytes.length != 33) {
        console.log("ERROR: Wrong payload length");
        return obj;
    }

    // check for Parametric TCR V2 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x02) {

        obj.SBX_BATT = (bytes[3] << 8) | (bytes[4]);     // battery voltage when equiped with an SBX solar charger 0-65535mV
        obj.SBX_PV = (bytes[5] << 8) | (bytes[6]);      // Solar panel power when equiped with SBX 0…65535 mW

        var temp = (bytes[7] << 8) | (bytes[8]);
        temp = bin16dec(temp);
        obj.TEMP = Math.floor(temp / 10);               // CPU Temperature

        obj.L0_CNT = (bytes[9] << 8) | (bytes[10]);      // object count from left in speed class 0, 0-65535
        obj.L0_AVG = bytes[11];                         // average speed from left in speed class 0, 0-65535
        obj.R0_CNT = (bytes[12] << 8) | (bytes[13]);    // object count from right in speed class 0, 0-65535
        obj.R0_AVG = bytes[14];                         // average speed from right in speed class 0, 0-65535

        obj.L1_CNT = (bytes[15] << 8) | (bytes[16]);    // object count from left in speed class 1, 0-65535
        obj.L1_AVG = bytes[17];                         // average speed from left in speed class 1, 0-65535
        obj.R1_CNT = (bytes[18] << 8) | (bytes[19]);    // object count from right in speed class 1, 0-65535
        obj.R1_AVG = bytes[20];                         // average speed from right in speed class 1, 0-65535

        obj.L2_CNT = (bytes[21] << 8) | (bytes[22]);    // object count from left in speed class 1, 0-65535
        obj.L2_AVG = bytes[23];                         // average speed from left in speed class 1, 0-65535
        obj.R2_CNT = (bytes[24] << 8) | (bytes[25]);    // object count from right in speed class 1, 0-65535
        obj.R2_AVG = bytes[26];                         // average speed from right in speed class 1, 0-65535

        obj.L3_CNT = (bytes[27] << 8) | (bytes[28]);    // object count from left in speed class 1, 0-65535
        obj.L3_AVG = bytes[29];                         // average speed from left in speed class 1, 0-65535
        obj.R3_CNT = (bytes[30] << 8) | (bytes[31]);    // object count from right in speed class 1, 0-65535
        obj.R3_AVG = bytes[32];                         // average speed from right in speed class 1, 0-65535
    }
    else {
        console.log("ERROR: TCR application payload V2 should start with be0202..  ");
    }
    return obj;
}

/**
 * Configuration payload decoder v1
 * @param {byteArray} bytes 
 * @param {int} port 
 */
function config_payload_v1_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        console.log("ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.");
        return obj;
    }

    if (bytes.length != 27) {
        console.log("ERROR: Wrong payload length");
        return obj;
    }

    // check for Parametric TCR V1 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x01) {

        obj.DeviceType = bytes[3];                      // 00: TCR, 01: TCR-S
        obj.Firmware = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version
        obj.OperatingMode = bytes[7];                   // 00: Timespan, 01: Trigger
        obj.DeviceClass = bytes[8];                     // 00: Class A, 02: Class C
        obj.UplinkType = bytes[9];                     // 00: Uncofirmed, 01: Confirmed
        obj.UplinkInterval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes
        obj.LinkCheckInterval = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes
        obj.HoldoffTime = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Minutes
        obj.RadarSensitivity = bytes[16];                          // 00: Uncofirmed, 01: Confirmed

        obj.LTRLaneDist = bytes[17];                               // Distance to lane with traffic from left
        obj.RTLLaneDist = bytes[18];                               // Distance to lane with traffic from right

        obj.SCO_START = bytes[19];                                 // Speed class 0 window start 0-255 km/h
        obj.SCO_END = bytes[20];                                   // Speed class 0 window end 0-255 km/h
        obj.SC1_START = bytes[21];                                 // Speed class 1 window start 0-255 km/h
        obj.SC1_END = bytes[22];                                   // Speed class 1 window end 0-255 km/h
        obj.SC2_START = bytes[23];                                 // Speed class 2 window start 0-255 km/h
        obj.SC2_END = bytes[24];                                   // Speed class 2 window end 0-255 km/h
        obj.SC3_START = bytes[25];                                 // Speed class 3 window start 0-255 km/h
        obj.SC3_END = bytes[26];                                   // Speed class 3 window end 0-255 km/h

    }
    else {
        console.log("ERROR: TCR configuration payload V1 should start with be0201..  ");
    }
    return obj;
}

/**
 * Configuration payload decoder v2
 * @param {byteArray} bytes 
 * @param {int} port 
 */
function config_payload_v2_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        console.log("ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.");
        return obj;
    }

    if (bytes.length != 29) {
        console.log("ERROR: Wrong payload length");
        return obj;
    }

    // check for Parametric TCR V2 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x02) {

        obj.DeviceType = bytes[3];                      // 00: TCR-LS, 01: TCR-LSS
        obj.Firmware = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version
        obj.OperatingMode = bytes[7];                   // 00: Timespan, 01: Trigger
        obj.DeviceClass = bytes[8];                     // 00: Class A, 02: Class C
        obj.UplinkType = bytes[9];                     // 00: Uncofirmed, 01: Confirmed
        obj.UplinkInterval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes
        obj.LinkCheckInterval = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes
        obj.HoldoffTime = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Minutes
        obj.RadarSensitivity = bytes[16];                          // 00: Uncofirmed, 01: Confirmed

        obj.LTRLaneDist = (bytes[17] << 8) | (bytes[18]);          // Distance to lane with traffic from left
        obj.RTLLaneDist = (bytes[19] << 8) | (bytes[20]);          // Distance to lane with traffic from right

        obj.SCO_START = bytes[21];                                 // Speed class 0 window start 0-255 km/h
        obj.SCO_END = bytes[22];                                   // Speed class 0 window end 0-255 km/h
        obj.SC1_START = bytes[23];                                 // Speed class 1 window start 0-255 km/h
        obj.SC1_END = bytes[24];                                   // Speed class 1 window end 0-255 km/h
        obj.SC2_START = bytes[25];                                 // Speed class 2 window start 0-255 km/h
        obj.SC2_END = bytes[26];                                   // Speed class 2 window end 0-255 km/h
        obj.SC3_START = bytes[27];                                 // Speed class 3 window start 0-255 km/h
        obj.SC3_END = bytes[28];                                   // Speed class 3 window end 0-255 km/h

    }
    else {
        console.log("ERROR: TCR configuration payload V2 should start with be0202..  ");
    }
    return obj;
}

/**
 * Configuration payload decoder v3
 * @param {byteArray} bytes 
 * @param {int} port 
 */
function config_payload_v3_decoder(bytes, port) {
    var obj = {};

    if (port != 190) {
        console.log("ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.");
        return obj;
    }

    if (bytes.length != 33) {
        console.log("ERROR: Wrong payload length");
        return obj;
    }

    // check for Parametric TCR V3 Payload
    if (bytes[0] == 0xbe && bytes[1] == 0x02 && bytes[2] == 0x03) {

        obj.DeviceType = bytes[3];                      // 00: TCR-LS, 01: TCR-LSS
        obj.Firmware = bytes[4] + "." + bytes[5] + "." + bytes[6];  // Firmware Major Version
        obj.OperatingMode = bytes[7];                   // 00: Timespan, 01: Trigger
        obj.DeviceClass = bytes[8];                     // 00: Class A, 02: Class C
        obj.UplinkType = bytes[9];                     // 00: Uncofirmed, 01: Confirmed
        obj.UplinkInterval = (bytes[10] << 8) | (bytes[11]);       // 1-1440 Minutes
        obj.LinkCheckInterval = (bytes[12] << 8) | (bytes[13]);    // 1-1440 Minutes
        obj.HoldoffTime = (bytes[14] << 8) | (bytes[15]);          // 1-1440 Minutes
        obj.RadarAutotuning = bytes[16];                           // 00: Autotuning off, 01: Autotuning active
        obj.RadarSensitivity = bytes[17];                          // 00: Uncofirmed, 01: Confirmed

        obj.LTRLaneDist = (bytes[18] << 8) | (bytes[19]);          // Distance to lane with traffic from left
        obj.RTLLaneDist = (bytes[20] << 8) | (bytes[21]);          // Distance to lane with traffic from right

        obj.SCO_START = bytes[22];                                 // Speed class 0 window start 0-255 km/h
        obj.SCO_END = bytes[23];                                   // Speed class 0 window end 0-255 km/h
        obj.SC1_START = bytes[24];                                 // Speed class 1 window start 0-255 km/h
        obj.SC1_END = bytes[25];                                   // Speed class 1 window end 0-255 km/h
        obj.SC2_START = bytes[26];                                 // Speed class 2 window start 0-255 km/h
        obj.SC2_END = bytes[27];                                   // Speed class 2 window end 0-255 km/h
        obj.SC3_START = bytes[28];                                 // Speed class 3 window start 0-255 km/h
        obj.SC3_END = bytes[29];                                   // Speed class 3 window end 0-255 km/h
        obj.SBX_Firmware = bytes[30] + "." + bytes[31] + "." + bytes[32];  // SBX Solar Charger Firmware Version 

    }
    else {
        console.log("ERROR: TCR configuration payload V3 should start with be0203..  ");
    }
    return obj;
}


/**
 * Two's complement
 * @param {int} bin 
 */
function bin16dec(bin) {
    var num = bin & 0xFFFF;
    if (0x8000 & num) num = -(0x010000 - num);
    return num;
}

/**
 * Converts hex String to byteArray
 * @param {String} hex 
 */
function hexToBytes(hex) {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
}

/**
 * Generic decoder which make us of the decoders above
 * @param {String} payload 
 * @param {int} port 
 */
function decode(payload, port) {
    var bytes = hexToBytes(payload);
    var obj = {};

    if (bytes[0] == 0xbe && bytes[1] == 0x02) {
        // its a TCR

        if (port == 15) {
            // it's an Application Payload

            if (bytes[2] == 0x01)    // V1
            {
                obj = app_payload_v1_decoder(bytes, port);
            }
            if (bytes[2] == 0x02)    // V2
            {
                obj = app_payload_v2_decoder(bytes, port);
            }
        }

        if (port == 190) {
            // it's a Configuration Payload

            if (bytes[2] == 0x01)    // V1
            {
                obj = config_payload_v1_decoder(bytes, port);
            }
            if (bytes[2] == 0x02)    // V2
            {
                obj = config_payload_v2_decoder(bytes, port);
            }
            if (bytes[2] == 0x03)    // V3
            {
                obj = config_payload_v3_decoder(bytes, port);
            }
        }
    }

    return obj;
}


// EXAMPLES

console.log('TCR application payload decoder v1: ')
console.log(JSON.stringify(decode('be02016412c218b800000000010600000000020b00000000011e000000000000', 15), null, 2));

console.log('TCR application payload decoder v2: ')
console.log(JSON.stringify(decode('be02021cc0000000a0000108000000000000000000000000000000000000000000', 15), null, 2));

console.log('TCR configuration payload decoder v1: ')
console.log(JSON.stringify(decode('be020100010000000000000305a0000064050f010708191a313278', 190), null, 2));

console.log('TCR configuration payload decoder v2: ')
console.log(JSON.stringify(decode('be020200010201000000000a05a000006400fa01c2010708191a313278', 190), null, 2));

console.log('TCR configuration payload decoder v3: ')
console.log(JSON.stringify(decode('be020300010300000001000a05a00000005a00fa00fa0107082800000000040100', 190), null, 2));
