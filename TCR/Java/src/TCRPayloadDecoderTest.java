import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TCRPayloadDecoderTest {
    private TCRPayloadDecoder decoder;

    /**
     * Initialize decoder
     */
    @BeforeEach
    public void initializeDecoder(){
        decoder = new TCRPayloadDecoder();
    }

    /**
     * Test application payload decoder v1
     */
    @Test
    public void testAppPayloadV1Decoder(){
        assertEquals(decoder.decode("be02016412c218b800000000010600000000020b00000000011e000000000000", 15).toString(),"{L1_CNT=0, R3_CNT=0, L2_AVG=0, R1_CNT=2, R2_AVG=30, R0_AVG=6, L2_CNT=0, L0_CNT=0, R2_CNT=1, SBX_PV=4802, L3_AVG=0, TEMP=632, SBX_BATT=100, R0_CNT=1, L1_AVG=0, L0_AVG=0, L3_CNT=0, R3_AVG=0, R1_AVG=11}");
    }

    /**
     * Test configuration payload decoder v1
     */
    @Test
    public void testConfigPayloadV1Decoder(){
        assertEquals(decoder.decode("be020100010000000000000305a0000064050f010708191a313278", 190).toString(),"{UplinkInterval=3, OperatingMode=0, SC1_End=25, DeviceClass=0, SC3_Start=50, SC3_End=120, SC0_Start=1, FirmwareVersion=1.0.0, UplinkType=0, SC1_Start=8, DeviceType=0, RadarSensitivity=100, SC0_End=7, HoldoffTime=0, LTRLaneDist=5, SC2_End=49, RTLLaneDist=15, LinkCheckInterval=1440, SC2_Start=26}");
    }

    /**
     * Test application payload decoder v2
     */
    @Test
    public void testAppPayloadV2Decoder(){
        assertEquals(decoder.decode("be0202001200000000000003000000000000000000000000000000000000000000", 15).toString(),"{L1_CNT=0, R3_CNT=0, L2_AVG=0, R1_CNT=0, R2_AVG=0, R0_AVG=0, L2_CNT=0, L0_CNT=0, R2_CNT=0, SBX_PV=0, L3_AVG=0, TEMP=0, SBX_BATT=18, R0_CNT=0, L1_AVG=0, L0_AVG=3, L3_CNT=0, R3_AVG=0, R1_AVG=0}");
    }

    /**
     * Test configuration payload decoder v2
     */
    @Test
    public void testConfigPayloadV2Decoder(){
        assertEquals(decoder.decode("be020201010101000001000a05a000006400fa00fa0107082800000000", 190).toString(),"{UplinkInterval=10, OperatingMode=0, SC1_End=40, DeviceClass=0, SC3_Start=0, SC3_End=0, SC0_Start=1, FirmwareVersion=1.1.1, UplinkType=1, SC1_Start=8, DeviceType=1, RadarSensitivity=100, SC0_End=7, HoldoffTime=0, LTRLaneDist=250, SC2_End=0, RTLLaneDist=250, LinkCheckInterval=1440, SC2_Start=0}");
    }

    /**
     * Test configuration payload decoder v3
     */
    @Test
    public void testConfigPayloadV3Decoder(){
        assertEquals(decoder.decode("be020300010300000001000a05a00000005a00fa00fa0107082800000000040100", 190).toString(),"{UplinkInterval=10, OperatingMode=0, SC1_End=40, DeviceClass=0, SC3_Start=0, SC3_End=0, SC0_Start=1, FirmwareVersion=1.3.0, UplinkType=1, SC1_Start=8, DeviceType=0, RadarSensitivity=90, SC0_End=7, HoldoffTime=0, LTRLaneDist=250, SC2_End=0, RTLLaneDist=250, LinkCheckInterval=1440, SC2_Start=0, RadarAutotuning=0, SBX_Firmware=4.1.0}");
    }
}