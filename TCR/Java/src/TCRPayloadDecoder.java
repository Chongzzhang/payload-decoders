/*
 TCR Payload Decoders

 THIS SOFTWARE IS PROVIDED BY PARAMETRIC GMBH AND ITS CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class TCRPayloadDecoder {


    /**
     * Application payload decoder v1
     *
     * @param payloadHex String
     * @param port       int
     * @return Map with decoded values
     */
    private Map<String, Integer> appPayloadV1Decoder(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Integer> decodedPayload = new HashMap<>();

        if (port != 15) {
            System.out.println("ERROR: Wrong port! TCR devices are using port 15 for application payloads.");
            return Collections.emptyMap();
        }
        if (byteArray.length != 32) {
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x01) {

            decodedPayload.put("SBX_BATT", byteArray[3]);
            decodedPayload.put("SBX_PV", (byteArray[4] << 8 | byteArray[5]));
            int temp = (byteArray[6] << 8) | byteArray[7];
            decodedPayload.put("TEMP", Math.floorDiv(bin16dec(temp), 10));

            decodedPayload.put("L0_CNT", (byteArray[8] << 8 | byteArray[9]));
            decodedPayload.put("L0_AVG", byteArray[10]);
            decodedPayload.put("R0_CNT", (byteArray[11] << 8 | byteArray[12]));
            decodedPayload.put("R0_AVG", byteArray[13]);

            decodedPayload.put("L1_CNT", (byteArray[14] << 8 | byteArray[15]));
            decodedPayload.put("L1_AVG", byteArray[16]);
            decodedPayload.put("R1_CNT", (byteArray[17] << 8 | byteArray[18]));
            decodedPayload.put("R1_AVG", byteArray[19]);

            decodedPayload.put("L2_CNT", (byteArray[20] << 8 | byteArray[21]));
            decodedPayload.put("L2_AVG", byteArray[22]);
            decodedPayload.put("R2_CNT", (byteArray[23] << 8 | byteArray[24]));
            decodedPayload.put("R2_AVG", byteArray[25]);

            decodedPayload.put("L3_CNT", (byteArray[26] << 8 | byteArray[27]));
            decodedPayload.put("L3_AVG", byteArray[28]);
            decodedPayload.put("R3_CNT", (byteArray[29] << 8 | byteArray[30]));
            decodedPayload.put("R3_AVG", byteArray[31]);

        } else {
            System.out.println("ERROR: TCR application payload V1 should start with be0201..");
        }

        return decodedPayload;
    }

    /**
     * Configuration payload decoder v1
     *
     * @param payloadHex String
     * @param port       int
     * @return Map with decoded values
     */
    private Map<String, Object> configPayloadV1Decoder(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if (port != 190) {
            System.out.println("ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.");
            return Collections.emptyMap();
        }

        if (byteArray.length != 27) {
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x01) {

            decodedPayload.put("DeviceType", byteArray[3]);
            decodedPayload.put("FirmwareVersion", byteArray[4] + "." + byteArray[5] + "." + byteArray[6]);
            decodedPayload.put("OperatingMode", byteArray[7]);
            decodedPayload.put("DeviceClass", byteArray[8]);
            decodedPayload.put("UplinkType", byteArray[9]);
            decodedPayload.put("UplinkInterval", (byteArray[10] << 8 | byteArray[11]));
            decodedPayload.put("LinkCheckInterval", (byteArray[12] << 8 | byteArray[13]));
            decodedPayload.put("HoldoffTime", (byteArray[14] << 8 | byteArray[15]));
            decodedPayload.put("RadarSensitivity", byteArray[16]);
            decodedPayload.put("LTRLaneDist", byteArray[17]);
            decodedPayload.put("RTLLaneDist", byteArray[18]);
            decodedPayload.put("SC0_Start", byteArray[19]);
            decodedPayload.put("SC0_End", byteArray[20]);
            decodedPayload.put("SC1_Start", byteArray[21]);
            decodedPayload.put("SC1_End", byteArray[22]);
            decodedPayload.put("SC2_Start", byteArray[23]);
            decodedPayload.put("SC2_End", byteArray[24]);
            decodedPayload.put("SC3_Start", byteArray[25]);
            decodedPayload.put("SC3_End", byteArray[26]);

        } else {
            System.out.println("ERROR: TCR configuration payload V1 should start with be0201..");
        }

        return decodedPayload;
    }

    /**
     * Application payload decoder v2
     *
     * @param payloadHex String
     * @param port       int
     * @return Map with decoded values
     */
    private Map<String, Integer> appPayloadV2Decoder(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Integer> decodedPayload = new HashMap<>();

        if (port != 15) {
            System.out.println("ERROR: Wrong port! TCR devices are using port 15 for application payloads.");
            return Collections.emptyMap();
        }
        if (byteArray.length != 33) {
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x02) {

            decodedPayload.put("SBX_BATT", (byteArray[3] << 8 | byteArray[4]));
            decodedPayload.put("SBX_PV", (byteArray[5] << 8 | byteArray[6]));
            int temp = (byteArray[7] << 8) | byteArray[8];
            decodedPayload.put("TEMP", Math.floorDiv(bin16dec(temp), 10));

            decodedPayload.put("L0_CNT", (byteArray[9] << 8 | byteArray[10]));
            decodedPayload.put("L0_AVG", byteArray[11]);
            decodedPayload.put("R0_CNT", (byteArray[12] << 8 | byteArray[13]));
            decodedPayload.put("R0_AVG", byteArray[14]);

            decodedPayload.put("L1_CNT", (byteArray[15] << 8 | byteArray[16]));
            decodedPayload.put("L1_AVG", byteArray[17]);
            decodedPayload.put("R1_CNT", (byteArray[18] << 8 | byteArray[19]));
            decodedPayload.put("R1_AVG", byteArray[20]);

            decodedPayload.put("L2_CNT", (byteArray[21] << 8 | byteArray[22]));
            decodedPayload.put("L2_AVG", byteArray[23]);
            decodedPayload.put("R2_CNT", (byteArray[24] << 8 | byteArray[25]));
            decodedPayload.put("R2_AVG", byteArray[26]);

            decodedPayload.put("L3_CNT", (byteArray[27] << 8 | byteArray[28]));
            decodedPayload.put("L3_AVG", byteArray[29]);
            decodedPayload.put("R3_CNT", (byteArray[30] << 8 | byteArray[31]));
            decodedPayload.put("R3_AVG", byteArray[32]);

        } else {
            System.out.println("ERROR: TCR application payload V2 should start with be0202..");
        }

        return decodedPayload;
    }

    /**
     * Configuration payload decoder v2
     *
     * @param payloadHex String
     * @param port       int
     * @return Map with decoded values
     */
    private Map<String, Object> configPayloadV2Decoder(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if (port != 190) {
            System.out.println("ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.");
            return Collections.emptyMap();
        }

        if (byteArray.length != 29) {
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x02) {

            decodedPayload.put("DeviceType", byteArray[3]);
            decodedPayload.put("FirmwareVersion", byteArray[4] + "." + byteArray[5] + "." + byteArray[6]);
            decodedPayload.put("OperatingMode", byteArray[7]);
            decodedPayload.put("DeviceClass", byteArray[8]);
            decodedPayload.put("UplinkType", byteArray[9]);
            decodedPayload.put("UplinkInterval", (byteArray[10] << 8 | byteArray[11]));
            decodedPayload.put("LinkCheckInterval", (byteArray[12] << 8 | byteArray[13]));
            decodedPayload.put("HoldoffTime", (byteArray[14] << 8 | byteArray[15]));
            decodedPayload.put("RadarSensitivity", byteArray[16]);
            decodedPayload.put("LTRLaneDist", (byteArray[17] << 8 | byteArray[18]));
            decodedPayload.put("RTLLaneDist", (byteArray[19] << 8 | byteArray[20]));
            decodedPayload.put("SC0_Start", byteArray[21]);
            decodedPayload.put("SC0_End", byteArray[22]);
            decodedPayload.put("SC1_Start", byteArray[23]);
            decodedPayload.put("SC1_End", byteArray[24]);
            decodedPayload.put("SC2_Start", byteArray[25]);
            decodedPayload.put("SC2_End", byteArray[26]);
            decodedPayload.put("SC3_Start", byteArray[27]);
            decodedPayload.put("SC3_End", byteArray[28]);

        } else {
            System.out.println("ERROR: TCR configuration payload V2 should start with be0202..");
        }

        return decodedPayload;
    }

    /**
     * Configuration payload decoder v2
     *
     * @param payloadHex String
     * @param port       int
     * @return Map with decoded values
     */
    private Map<String, Object> configPayloadV3Decoder(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if (port != 190) {
            System.out.println("ERROR: Wrong port! TCR devices are using port 190 for configuration payloads.");
            return Collections.emptyMap();
        }

        if (byteArray.length != 33) {
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x03) {

            decodedPayload.put("DeviceType", byteArray[3]);
            decodedPayload.put("FirmwareVersion", byteArray[4] + "." + byteArray[5] + "." + byteArray[6]);
            decodedPayload.put("OperatingMode", byteArray[7]);
            decodedPayload.put("DeviceClass", byteArray[8]);
            decodedPayload.put("UplinkType", byteArray[9]);
            decodedPayload.put("UplinkInterval", (byteArray[10] << 8 | byteArray[11]));
            decodedPayload.put("LinkCheckInterval", (byteArray[12] << 8 | byteArray[13]));
            decodedPayload.put("HoldoffTime", (byteArray[14] << 8 | byteArray[15]));
            decodedPayload.put("RadarAutotuning", byteArray[16]);
            decodedPayload.put("RadarSensitivity", byteArray[17]);
            decodedPayload.put("LTRLaneDist", (byteArray[18] << 8 | byteArray[19]));
            decodedPayload.put("RTLLaneDist", (byteArray[20] << 8 | byteArray[21]));
            decodedPayload.put("SC0_Start", byteArray[22]);
            decodedPayload.put("SC0_End", byteArray[23]);
            decodedPayload.put("SC1_Start", byteArray[24]);
            decodedPayload.put("SC1_End", byteArray[25]);
            decodedPayload.put("SC2_Start", byteArray[26]);
            decodedPayload.put("SC2_End", byteArray[27]);
            decodedPayload.put("SC3_Start", byteArray[28]);
            decodedPayload.put("SC3_End", byteArray[29]);
            decodedPayload.put("SBX_Firmware", byteArray[30] + "." + byteArray[31] + "." + byteArray[32]);

        } else {
            System.out.println("ERROR: TCR configuration payload V3 should start with be0203..");
        }

        return decodedPayload;
    }

    /**
     * Converts hex String to byteArray
     *
     * @param payloadHex String
     * @return ByteArray
     */
    private int[] hexToBytes(String payloadHex) {
        int[] arr = new int[payloadHex.length() / 2];
        int counter = 0;
        for (int i = 0; i < payloadHex.length(); i += 2) {
            arr[counter] = Integer.parseInt(payloadHex.substring(i, i + 2), 16);
            counter += 1;
        }
        return arr;
    }

    /**
     * Two's complement
     *
     * @param bin int
     * @return int
     */
    private int bin16dec(int bin) {
        int num = bin & 0xFFFF;
        if ((0x8000 & num) == 0x8000) num = -(0x010000 - num);
        return num;
    }

    /**
     * Generic decoder - make use of the concrete decoder above
     *
     * @param payload String
     * @param port    int
     * @return Map with decoded values
     */
    public Map<String, ?> decode(String payload, int port) {
        Map<String, ?> result = null;
        int[] byteArray = hexToBytes(payload);

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x01) {
            if (port == 15) {
                result = appPayloadV1Decoder(payload, port);
            } else if (port == 190) {
                result = configPayloadV1Decoder(payload, port);
            }
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x02) {
            if (port == 15) {
                result = appPayloadV2Decoder(payload, port);
            } else if (port == 190) {
                result = configPayloadV2Decoder(payload, port);
            }
        }

        if (byteArray[0] == 0xbe && byteArray[1] == 0x02 && byteArray[2] == 0x03) {
            if (port == 190) {
                result = configPayloadV3Decoder(payload, port);
            }
        }

        if (result == null) {
            System.out.println("ERROR");
            result = Collections.emptyMap();
        }

        return result;
    }
}
