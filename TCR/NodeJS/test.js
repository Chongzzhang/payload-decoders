
var decoder = require("./tcr_payload_decoder.js");

// test the TCR application payload decoder (v1)
console.log(JSON.stringify(decoder.decode('be02016412c218b800000000010600000000020b00000000011e000000000000', 15), null, 2));
/*

{
  "SBX_BATT": 100,
  "SBX_PV": 4802,
  "TEMP": 632,
  "L0_CNT": 0,
  "L0_AVG": 0,
  "R0_CNT": 1,
  "R0_AVG": 6,
  "L1_CNT": 0,
  "L1_AVG": 0,
  "R1_CNT": 2,
  "R1_AVG": 11,
  "L2_CNT": 0,
  "L2_AVG": 0,
  "R2_CNT": 1,
  "R2_AVG": 30,
  "L3_CNT": 0,
  "L3_AVG": 0,
  "R3_CNT": 0,
  "R3_AVG": 0
}

*/

// test the TCR application payload decoder (v2)
console.log(JSON.stringify(decoder.decode('be02021cc0000000a0000108000000000000000000000000000000000000000000', 15), null, 2));
/*

{
   "SBX_BATT": 7360,
  "SBX_PV": 0,
  "TEMP": 16,
  "L0_CNT": 1,
  "L0_AVG": 8,
  "R0_CNT": 0,
  "R0_AVG": 0,
  "L1_CNT": 0,
  "L1_AVG": 0,
  "R1_CNT": 0,
  "R1_AVG": 0,
  "L2_CNT": 0,
  "L2_AVG": 0,
  "R2_CNT": 0,
  "R2_AVG": 0,
  "L3_CNT": 0,
  "L3_AVG": 0,
  "R3_CNT": 0,
  "R3_AVG": 0
}

*/


// test the TCR config payload decoder (v1)
console.log(JSON.stringify(decoder.decode('be020100010000000000000305a0000064050f010708191a313278', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.0.0",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 0,
  "UplinkInterval": 3,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarSensitivity": 100,
  "LTRLaneDist": 5,
  "RTLLaneDist": 15,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 25,
  "SC2_START": 26,
  "SC2_END": 49,
  "SC3_START": 50,
  "SC3_END": 120
}
*/

// test the TCR config payload decoder (v2)
console.log(JSON.stringify(decoder.decode('be020200010201000000000a05a000006400fa01c2010708191a313278', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.2.1",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 0,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarSensitivity": 100,
  "LTRLaneDist": 250,
  "RTLLaneDist": 450,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 25,
  "SC2_START": 26,
  "SC2_END": 49,
  "SC3_START": 50,
  "SC3_END": 120
}
*/


// test the TCR config payload decoder (v3)
console.log(JSON.stringify(decoder.decode('be020300010300000001000a05a00000005a00fa00fa0107082800000000040100', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.3.0",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 1,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarAutotuning": 0,
  "RadarSensitivity": 90,
  "LTRLaneDist": 250,
  "RTLLaneDist": 250,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 40,
  "SC2_START": 0,
  "SC2_END": 0,
  "SC3_START": 0,
  "SC3_END": 0,
  "SBX_Firmware": "4.1.0"
}
*/

// test the TCR config payload decoder (v4)
console.log(JSON.stringify(decoder.decode('be 02 04 00 01 04 00 00 00 01 00 0a 05 a0 00 00 00 50 00 64 01 90 01 28 00 5a 01 07 08 28 00 00 00 00 00 fa 00 fa 00 00 00 00', 190), null, 2));
/*
{
  "DeviceType": 0,
  "Firmware": "1.3.0",
  "OperatingMode": 0,
  "DeviceClass": 0,
  "UplinkType": 1,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "HoldoffTime": 0,
  "RadarAutotuning": 0,
  "RadarSensitivity": 90,
  "LTRLaneDist": 250,
  "RTLLaneDist": 250,
  "SCO_START": 1,
  "SCO_END": 7,
  "SC1_START": 8,
  "SC1_END": 40,
  "SC2_START": 0,
  "SC2_END": 0,
  "SC3_START": 0,
  "SC3_END": 0,
  "SBX_Firmware": "4.1.0"
}
*/