![Parametric GmbH](https://www.parametric.ch/assets/images/logos/logo.png)

# Payload Decoders for Parametric LoRaWAN Devices

These code samples are provided free of charge as a starting point to write your own decoders.

| Device | Languages |
|--------|-----------|
| **PCR2**   | [NodeJS](https://bitbucket.org/parametricengineering/payload-decoders/src/master/PCR2/NodeJS/), [JavaScript](https://bitbucket.org/parametricengineering/payload-decoders/src/master/PCR2/JavaScript/), [Python](https://bitbucket.org/parametricengineering/payload-decoders/src/master/PCR2/Python/), [Java](https://bitbucket.org/parametricengineering/payload-decoders/src/master/PCR2/Java/src/)   |
| **TCR**    |  [NodeJS](https://bitbucket.org/parametricengineering/payload-decoders/src/master/TCR/NodeJS/), [JavaScript](https://bitbucket.org/parametricengineering/payload-decoders/src/master/TCR/JavaScript/), [Python](https://bitbucket.org/parametricengineering/payload-decoders/src/master/TCR/Python/), [Java](https://bitbucket.org/parametricengineering/payload-decoders/src/master/TCR/Java/src/)          |


Contribution to this repository is highly welcome.

