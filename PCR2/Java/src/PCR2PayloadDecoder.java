/*
   PCR2 Payload Decoder

  THIS SOFTWARE IS PROVIDED BY PARAMETRIC GMBH AND ITS CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
  OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

  */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PCR2PayloadDecoder {

    // Version 2

    /**
     * Elsys payload decoder
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Integer> decodeElsysPl(String payloadHex, int port){
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Integer> decodedPayload = new HashMap<>();

        if(port != 14){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 14 for application payloads.");
            return Collections.emptyMap();
        }

        int i;
        for(i = 0; i < byteArray.length; i++){
            switch (byteArray[i]){
                case 0x01:
                    int temp = (byteArray[i+1] << 8) | byteArray[i+2];
                    decodedPayload.put("TEMP", Math.floorDiv(bin16dec(temp),10));
                    i +=2;
                    break;
                case 0x0A:
                    decodedPayload.put("LTR", (byteArray[i+1] << 8) | byteArray[i+2]);
                    i +=2;
                    break;
                case 0x16:
                    decodedPayload.put("RTL", (byteArray[i+1] << 8) | byteArray[i+2]);
                    i += 2;
                    break;
                default:
                    i = byteArray.length;
                    break;
            }
        }

        return decodedPayload;
    }

    /**
     * Configuration payload decoder v2
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Object> decodeV2ConfigPayload(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if(port != 190){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.");
            return Collections.emptyMap();
        }

        if(byteArray.length != 14){
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        decodedPayload.put("DeviceType", byteArray[0]);
        decodedPayload.put("Firmware", byteArray[1] +"."+ byteArray[2] + "." + byteArray[3]);
        decodedPayload.put("OperationMode", byteArray[4]);
        decodedPayload.put("PayloadType", byteArray[5]);
        decodedPayload.put("UplinkType", byteArray[6]);
        decodedPayload.put("UplinkInterval", (byteArray[7] << 8 | byteArray[8]));
        decodedPayload.put("LinkCheckInterval", (byteArray[9] << 8 | byteArray[10]));
        decodedPayload.put("HoldoffTime", (byteArray[11] << 8 | byteArray[12]));
        decodedPayload.put("RadarSensitivity", byteArray[13]);

        return decodedPayload;
    }


    // Version 3

    /**
     * Extended application payload decoder v3
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Integer> decodeExtendedV3(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Integer> decodedPayload = new HashMap<>();

        if(port != 14){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 14 for application payloads.");
            return Collections.emptyMap();
        }

        if(byteArray.length != 16){
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x03) {

            decodedPayload.put("LTR", (byteArray[3] << 8 | byteArray[4]));
            decodedPayload.put("RTL", (byteArray[5] << 8 | byteArray[6]));
            decodedPayload.put("LTR_SUM", (byteArray[7] << 8 | byteArray[8]));
            decodedPayload.put("RTL_SUM", (byteArray[9] << 8 | byteArray[10]));
            decodedPayload.put("SBX_BATT", byteArray[11]);
            decodedPayload.put("SBX_PV", (byteArray[12] << 8 | byteArray[13]));
            decodedPayload.put("DIFF", Math.abs(decodedPayload.get("RTL_SUM") - decodedPayload.get("LTR_SUM")));
            int temp = (byteArray[14] << 8) | byteArray[15];
            decodedPayload.put("TEMP", Math.floorDiv(bin16dec(temp),10));

        }else {
            System.out.println("ERROR: PCR2 application payload V3 should start with be0103..");
        }

        return decodedPayload;
    }

    /**
     * Configuration payload decoder v3
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Object> decodeV3ConfigPayload(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if(port != 190){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.");
            return Collections.emptyMap();
        }

        if(byteArray.length != 20){
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x03) {

            decodedPayload.put("DeviceType", byteArray[3]);
            decodedPayload.put("Firmware", byteArray[4] + "." + byteArray[5] + "." + byteArray[6]);
            decodedPayload.put("OperationMode", byteArray[7]);
            decodedPayload.put("PayloadType", byteArray[8]);
            decodedPayload.put("DeviceClass", byteArray[9]);
            decodedPayload.put("UplinkType", byteArray[10]);
            decodedPayload.put("UplinkInterval", (byteArray[11] << 8 | byteArray[12]));
            decodedPayload.put("LinkCheckInterval", (byteArray[13] << 8 | byteArray[14]));
            decodedPayload.put("CapacityLimit", (byteArray[15] << 8 | byteArray[16]));
            decodedPayload.put("HoldoffTime", (byteArray[17] << 8 | byteArray[18]));
            decodedPayload.put("RadarSensitivity", byteArray[19]);

        }else {
            System.out.println("ERROR: PCR2 configuration payload V3 should start with be0103..");
        }

        return decodedPayload;
    }

    // Version 4

    /**
     * Application payload decoder v4
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Integer> decodeExtendedV4(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Integer> decodedPayload = new HashMap<>();

        if(port != 14){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 14 for application payloads.");
            return Collections.emptyMap();
        }

        if(byteArray.length != 17){
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x04) {

            decodedPayload.put("LTR", (byteArray[3] << 8 | byteArray[4]));
            decodedPayload.put("RTL", (byteArray[5] << 8 | byteArray[6]));
            decodedPayload.put("LTR_SUM", (byteArray[7] << 8 | byteArray[8]));
            decodedPayload.put("RTL_SUM", (byteArray[9] << 8 | byteArray[10]));
            decodedPayload.put("SBX_BATT", (byteArray[11] << 8 | byteArray[12]));
            decodedPayload.put("SBX_PV", (byteArray[13] << 8 | byteArray[14]));
            decodedPayload.put("DIFF", Math.abs(decodedPayload.get("RTL_SUM") - decodedPayload.get("LTR_SUM")));
            int temp = (byteArray[15] << 8) | byteArray[16];
            decodedPayload.put("TEMP", Math.floorDiv(bin16dec(temp),10));

        }else {
            System.out.println("ERROR: PCR2 application payload V4 should start with be0104..");
        }

        return decodedPayload;
    }

    /**
     * Configuration payload decoder v4
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Object> decodeV4ConfigPayload(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if(port != 190){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.");
            return Collections.emptyMap();
        }

        if(byteArray.length != 31){
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x04) {

            decodedPayload.put("DeviceType", byteArray[3]);
            decodedPayload.put("Firmware", byteArray[4] + "." + byteArray[5] + "." + byteArray[6]);
            decodedPayload.put("OperationMode", byteArray[7]);
            decodedPayload.put("PayloadType", byteArray[8]);
            decodedPayload.put("DeviceClass", byteArray[9]);
            decodedPayload.put("UplinkType", byteArray[10]);
            decodedPayload.put("UplinkInterval", (byteArray[11] << 8 | byteArray[12]));
            decodedPayload.put("LinkCheckInterval", (byteArray[13] << 8 | byteArray[14]));
            decodedPayload.put("CapacityLimit", (byteArray[15] << 8 | byteArray[16]));
            decodedPayload.put("HoldoffTime", (byteArray[17] << 8 | byteArray[18]));
            decodedPayload.put("InactivityTimeout", (byteArray[19] << 8 | byteArray[20]));
            decodedPayload.put("MountingDirection", byteArray[21]);
            decodedPayload.put("MountingTilt", byteArray[22]);
            decodedPayload.put("DetectionAngle", byteArray[23]);
            decodedPayload.put("MinDist", (byteArray[24] << 8 | byteArray[25]));
            decodedPayload.put("MaxDist", (byteArray[26] << 8 | byteArray[27]));
            decodedPayload.put("MinSpeed", byteArray[28]);
            decodedPayload.put("MaxSpeed", byteArray[29]);
            decodedPayload.put("RadarSensitivity", byteArray[30]);

        }else {
            System.out.println("ERROR: PCR2 configuration payload V4 should start with be0104..");
        }

        return decodedPayload;
    }

    // Version 4
    
    /**
     * Configuration payload decoder v5
     * @param payloadHex String
     * @param port int
     * @return Map with decoded values
     */
    private Map<String, Object> decodeV5ConfigPayload(String payloadHex, int port) {
        int[] byteArray = hexToBytes(payloadHex);
        Map<String, Object> decodedPayload = new HashMap<>();

        if(port != 190){
            System.out.println("ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.");
            return Collections.emptyMap();
        }

        if(byteArray.length != 34){
            System.out.println("ERROR: Wrong payload length");
            return Collections.emptyMap();
        }

        if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x05) {

            decodedPayload.put("DeviceType", byteArray[3]);
            decodedPayload.put("Firmware", byteArray[4] + "." + byteArray[5] + "." + byteArray[6]);
            decodedPayload.put("OperationMode", byteArray[7]);
            decodedPayload.put("PayloadType", byteArray[8]);
            decodedPayload.put("DeviceClass", byteArray[9]);
            decodedPayload.put("UplinkType", byteArray[10]);
            decodedPayload.put("UplinkInterval", (byteArray[11] << 8 | byteArray[12]));
            decodedPayload.put("LinkCheckInterval", (byteArray[13] << 8 | byteArray[14]));
            decodedPayload.put("CapacityLimit", (byteArray[15] << 8 | byteArray[16]));
            decodedPayload.put("HoldoffTime", (byteArray[17] << 8 | byteArray[18]));
            decodedPayload.put("InactivityTimeout", (byteArray[19] << 8 | byteArray[20]));
            decodedPayload.put("RadarEnabled", byteArray[21]);
            decodedPayload.put("BeamAngle", byteArray[22]);
            decodedPayload.put("MinDist", (byteArray[23] << 8 | byteArray[24]));
            decodedPayload.put("MaxDist", (byteArray[25] << 8 | byteArray[26]));
            decodedPayload.put("MinSpeed", byteArray[27]);
            decodedPayload.put("MaxSpeed", byteArray[28]);
            decodedPayload.put("RadarAutotune", byteArray[29]);
            decodedPayload.put("RadarSensitivity", byteArray[30]);
            decodedPayload.put("SBXVersion", byteArray[31] + "." + byteArray[32] + "." + byteArray[33]);

        }else {
            System.out.println("ERROR: PCR2 configuration payload V5 should start with be0105..");
        }

        return decodedPayload;
    }

    /**
     * Converts hex String to byteArray
     * @param payloadHex String
     * @return ByteArray
     */
    private int[] hexToBytes(String payloadHex){
        int[] arr = new int[payloadHex.length()/2];
        int counter = 0;
        for (int i=0; i < payloadHex.length(); i +=2){
            arr[counter] = Integer.parseInt(payloadHex.substring(i, i+2), 16);
            counter += 1;
        }
        return arr;
    }

    /**
     * Two's complement
     * @param bin int
     * @return int
     */
    private int bin16dec(int bin){
        int num = bin & 0xFFFF;
        if((0x8000 & num) == 0x8000) num = -(0x010000 - num);
        return num;
    }

    /**
     * Generic decoder - make use of the concrete decoder above
     * @param payload String
     * @param port int
     * @return Map with decoded values
     */
    public Map<String, ?> decode(String payload, int port){
        int[] byteArray = hexToBytes(payload);

        if(port == 14){
            if(byteArray[0] == 0x0a) {
                return decodeElsysPl(payload, port);
            }
            else if(byteArray[0] == 0 && byteArray[1] == 102){
                System.out.println("Decode lpp payload");
                return Collections.emptyMap();
            }
            else if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x03){
                return decodeExtendedV3(payload, port);
            }
            else if(byteArray[0] == 0xbe && byteArray[1] == 0x01 && byteArray[2] == 0x04){
                return decodeExtendedV4(payload, port);
            }
            else{
                System.out.println("ERROR: No decoder for application payload");
            }
        }

        if(port == 190){
            if(byteArray.length == 14){
                return decodeV2ConfigPayload(payload, port);
            }
            else if(byteArray.length == 20){
                return decodeV3ConfigPayload(payload, port);
            }
            else if(byteArray.length == 31){
                return decodeV4ConfigPayload(payload, port);
            }
            else if(byteArray.length == 34){
                return decodeV5ConfigPayload(payload, port):
            }
            else{
                System.out.println("ERROR: No decoder for config payload");
            }
        }
        return Collections.emptyMap();
    }

    public static void main(String[] args) {
        PCR2PayloadDecoder decoder = new PCR2PayloadDecoder();
        System.out.println(decoder.decode("be01040403060003020000000a05a00064000100b4005a50003201f4011450", 190));
    }

}
