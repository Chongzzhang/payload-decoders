import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class PCR2PayloadDecoderTest {
    private PCR2PayloadDecoder decoder;

    /**
     * Initialize decoder
     */
    @BeforeEach
    public void initializeDecoder(){
        decoder = new PCR2PayloadDecoder();
    }

    /**
     * Test elsys application payload decoder
     */
    @Test
    public void testDecodeElsysPl(){
        assertEquals(decoder.decode("0a0001160002010023", 14).toString(),"{TEMP=3, LTR=1, RTL=2}");
    }

    /**
     * Test configuration payload decoder v2
     */
    @Test
    public void testDecodeV2ConfigPayload(){
        assertEquals(decoder.decode("00030200000000000a05a0000050", 190).toString(),"{DeviceType=0, UplinkInterval=10, OperationMode=0, RadarSensitivity=80, HoldoffTime=0, LinkCheckInterval=1440, Firmware=3.2.0, PayloadType=0, UplinkType=0}");
    }

    /**
     * Test extended application payload v3
     */
    @Test
    public void testDecodeExtendedV3(){
        assertEquals(decoder.decode("be01030001000200030004640ce40139", 14).toString(),"{SBX_PV=3300, TEMP=31, SBX_BATT=100, DIFF=1, LTR_SUM=3, RTL_SUM=4, LTR=1, RTL=2}");
    }

    /**
     * Test configuration payload v3
     */
    @Test
    public void testDecodeV3ConfigPayload(){
        assertEquals(decoder.decode("be01030003040000000001000a05a00000000050", 190).toString(),"{DeviceType=0, UplinkInterval=10, OperationMode=0, RadarSensitivity=80, DeviceClass=0, HoldoffTime=0, LinkCheckInterval=1440, CapacityLimit=0, Firmware=3.4.0, PayloadType=0, UplinkType=1}");
    }

    /**
     * Test extended application payload v4
     */
    @Test
    public void testDecodeExtendedV4(){
        assertEquals(decoder.decode("be010400010002000300041c200ce40139", 14).toString(),"{SBX_PV=3300, TEMP=31, SBX_BATT=7200, DIFF=1, LTR_SUM=3, RTL_SUM=4, LTR=1, RTL=2}");
    }

    /**
     * Test configuration payload v4
     */
    @Test
    public void testDecodeV4ConfigPayload(){
        assertEquals(decoder.decode("be01040403060003020000000a05a00064000100b4005a50003201f4011450", 190).toString(),"{MaxSpeed=20, UplinkInterval=10, MinDist=50, OperationMode=3, DeviceClass=0, DetectionAngle=80, MountingTilt=90, MaxDist=500, Firmware=3.6.0, PayloadType=2, UplinkType=0, InactivityTimeout=180, DeviceType=4, RadarSensitivity=80, MinSpeed=1, HoldoffTime=1, LinkCheckInterval=1440, MountingDirection=0, CapacityLimit=100}");
    }

    /**
     * Test configuration payload v5
     */
    @Test
    public void testDecodeV5ConfigPayload(){
        assertEquals(decoder.decode("be010500030a0000020001000a05a00000000000780128003200c80107003c040101", 190).toString(),"{MaxSpeed=7, UplinkInterval=10, MinDist=50, OperationMode=0, DeviceClass=0, MaxDist=200, Firmware=3.10.0, PayloadType=2, UplinkType=1, InactivityTimeout=120, DeviceType=0, RadarSensitivity=60, MinSpeed=1, HoldoffTime=0, LinkCheckInterval=1440, CapacityLimit=0, SBXVersion=4.1.1, RadarEnabled=1, BeamAngle=40, RadarAutotune=0}");
    }

}