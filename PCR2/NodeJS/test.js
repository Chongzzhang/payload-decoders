
var decoder = require("./pcr2_payload_decoder.js");

// test the ELSYS app pl decoder
console.log("ELSYS Application Payload");
console.log(JSON.stringify(decoder.decode('0a0001160002010023',14), null, 2));

// test the extended app pl v3
console.log("Extended Application Payload V3");
console.log(JSON.stringify(decoder.decode('be01030001000200030004640ce40139',14), null, 2));

// test the extended app pl v4
console.log("Extended Application Payload V4");
console.log(JSON.stringify(decoder.decode('be010400010002000300041c200ce40139',14), null, 2));

// test the config pl v2
console.log("Config Payload V2");
console.log(JSON.stringify(decoder.decode('00030200000000000a05a0000050',190), null, 2))

// test the config pl v3
console.log("Config Payload V3");
console.log(JSON.stringify(decoder.decode('be01030003040000000001000a05a00000000050',190), null, 2))

// test the config pl v4
console.log("Config Payload V4");
console.log(JSON.stringify(decoder.decode('be01040403060003020000000a05a00064000100b4005a50003201f4011450',190), null, 2))

// test the config pl v5
console.log("Config Payload V5");
console.log(JSON.stringify(decoder.decode('be010500030a0100020001000a05a00000000000780128003200c80107005a040101',190), null, 2))
/*
Config Payload V5
{
  "DeviceType": 0,
  "Firmware": "3.10.1",
  "OperationMode": 0,
  "PayloadType": 2,
  "DeviceClass": 0,
  "UplinkType": 1,
  "UplinkInterval": 10,
  "LinkCheckInterval": 1440,
  "CapacityLimit": 0,
  "HoldoffTime": 0,
  "InactivityTimeout": 120,
  "RadarEnabled": 1,
  "BeamAngle": 40,
  "MinDist": 50,
  "MaxDist": 200,
  "MinSpeed": 1,
  "MaxSpeed": 7,
  "RadarAutotune": 0,
  "RadarSensitivity": 90,
  "SBXVersion": "4.1.1"
}
*/