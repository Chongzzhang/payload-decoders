import unittest
from .pcr2_payload_decoder import decode


class Test_PCR2_Payload_Decoder(unittest.TestCase):
    # Test elsys application payload decoder
    def test_decode_elysy_pl(self):
        decoded_payload = decode('0a0001160002010023', 14)
        self.assertEqual(decoded_payload, {'LTR': 1, 'RTL': 2, 'TEMP': 3})

    # Test configuration payload v2 decoder
    def test_decode_v2_config_payload(self):
        decoded_payload = decode('00030200000000000a05a0000050', 190)
        self.assertEqual(decoded_payload,
                         {'DeviceType': 0, 'Firmware': '3.2.0', 'OperationMode': 0, 'PayloadType': 0,
                          'UplinkType': 0, 'UplinkInterval': 10, 'LinkCheckInterval': 1440, 'HoldoffTime': 0,
                          'RadarSensitivity': 80})

    # Test extended application payload v3 decoder
    def test_decode_extended_v3(self):
        decoded_payload = decode('be01030001000200030004640ce40139', 14)
        self.assertEqual(decoded_payload,
                         {'LTR': 2, 'RTL': 1, 'LTR_SUM': 3, 'RTL_SUM': 4, 'SBX_BATT': 100, 'SBX_PV': 3300, 'DIFF': 1,
                          'TEMP': 31})

    # Test configuration payload v3 decoder
    def test_decode_v3_config_payload(self):
        decoded_payload = decode('be01030003040000000001000a05a00000000050', 190)
        self.assertEqual(decoded_payload,
                         {'DeviceType': 0, 'Firmware': '3.4.0', 'OperationMode': 0, 'PayloadType': 0,
                          'DeviceClass': 0, 'UplinkType': 1, 'UplinkInterval': 10, 'LinkCheckInterval': 1440,
                          'CapacityLimit': 0, 'HoldoffTime': 0, 'RadarSensitivity': 80})

    # Test extended application payload v4 decoder
    def test_decode_extended_v4(self):
        decoded_payload = decode('be010400010002000300041c200ce40139', 14)
        self.assertEqual(decoded_payload,
                         {'LTR': 2, 'RTL': 1, 'LTR_SUM': 3, 'RTL_SUM': 4, 'SBX_BATT': 7200, 'SBX_PV': 3300, 'DIFF': 1,
                          'TEMP': 31})

    # Test configuration payload v4 decoder
    def test_decode_v4_config_payload(self):
        decoded_payload = decode('be01040403060003020000000a05a00064000100b4005a50003201f4011450', 190)
        self.assertEqual(decoded_payload,
                         {'DeviceType': 4, 'Firmware': '3.6.0', 'OperationMode': 3, 'PayloadType': 2,
                          'DeviceClass': 0, 'UplinkType': 0, 'UplinkInterval': 10, 'LinkCheckInterval': 1440,
                          'CapacityLimit': 100, 'HoldoffTime': 1, 'InactivityTimeout': 180, 'MountingDirection': 0,
                          'MountingTilt': 90, 'DetectionAngle': 80, 'MinDist': 50, 'MaxDist': 500, 'MinSpeed': 1,
                          'MaxSpeed': 20, 'RadarSensitivity': 80})

    # Test configuration payload v5 decoder
        def test_decode_v5_config_payload(self):
            decoded_payload = decode('be010500030a0000020001000a05a00000000000780128003200c80107003c040101', 190)
            self.assertEqual(decoded_payload,
                            {'DeviceType': 0, 'Firmware': '3.10.0', 'OperationMode': 0, 'PayloadType': 2,
                            'DeviceClass': 0, 'UplinkType': 1, 'UplinkInterval': 10, 'LinkCheckInterval': 1440,
                            'CapacityLimit': 0,
                            'HoldoffTime': 0, 'InactivityTimeout': 120, 'RadarEnabled': 1,
                            'BeamAngle': 40, 'MinDist': 50, 'MaxDist': 200, 'MinSpeed': 1,
                            'MaxSpeed': 7, 'RadarAutotune': 0, 'RadarSensitivity': 60, 'SBXVersion': '4.1.1'})


if __name__ == '__main__':
    unittest.main()
