"""
  PCR2 Payload Decoder

  THIS SOFTWARE IS PROVIDED BY PARAMETRIC GMBH AND ITS CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL APPLE INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from math import floor

' Decoder Version 2 '


def decode_elsys_pl(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'LTR': '', 'RTL': '', 'TEMP': ''}

    if port != 14:
        print('ERROR: Wrong port! PCR2 devices are using port 14 for application payloads.')
        return None

    for x in range(len(bytes_)):
        if bytes_[x] == 0x01:
            temp = (bytes_[x + 1] << 8 | bytes_[x + 2])
            decoded_payload['TEMP'] = floor(bin16dec(temp) / 10)
        if bytes_[x] == 0x0A:
            decoded_payload['LTR'] = (bytes_[x + 1] << 8 | bytes_[x + 2])
        if bytes_[x] == 0x16:
            decoded_payload['RTL'] = (bytes_[x + 1] << 8) | (bytes_[x + 2])

    return decoded_payload


def decode_v2_config_payload(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'Firmware': '', 'OperationMode': '', 'PayloadType': '',
                       'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '', 'HoldoffTime': '',
                       'RadarSensitivity': ''}

    if port != 190:
        print('ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 14:
        print('ERROR: Wrong payload length')
        return None

    decoded_payload['DeviceType'] = bytes_[0]
    decoded_payload['Firmware'] = str(bytes_[1]) + '.' + str(bytes_[2]) + '.' + str(bytes_[3])
    decoded_payload['OperationMode'] = bytes_[4]
    decoded_payload['PayloadType'] = bytes_[5]
    decoded_payload['UplinkType'] = bytes_[6]
    decoded_payload['UplinkInterval'] = (bytes_[7] << 8 | bytes_[8])
    decoded_payload['LinkCheckInterval'] = (bytes_[9] << 8 | bytes_[10])
    decoded_payload['HoldoffTime'] = (bytes_[11] << 8 | bytes_[12])
    decoded_payload['RadarSensitivity'] = bytes_[13]

    return decoded_payload


' Decoder Version 3 '


def decode_extended_v3(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'LTR': '', 'RTL': '', 'LTR_SUM': '', 'RTL_SUM': '', 'SBX_BATT': '', 'SBX_PV': '', 'DIFF': '',
                       'TEMP': ''}

    if port != 14:
        print('ERROR: Wrong port! PCR2 devices are using port 14 for application payloads.')
        return None

    if len(bytes_) != 16:
        print('ERROR: Wrong payload length')
        return None

    if bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x03:

        decoded_payload['RTL'] = (bytes_[3] << 8 | bytes_[4])
        decoded_payload['LTR'] = (bytes_[5] << 8 | bytes_[6])
        decoded_payload['LTR_SUM'] = (bytes_[7] << 8 | bytes_[8])
        decoded_payload['RTL_SUM'] = (bytes_[9] << 8 | bytes_[10])
        decoded_payload['SBX_BATT'] = bytes_[11]
        decoded_payload['SBX_PV'] = (bytes_[12] << 8 | bytes_[13])
        decoded_payload['DIFF'] = abs(decoded_payload['LTR_SUM'] - decoded_payload['RTL_SUM'])
        temp = (bytes_[14] << 8) | (bytes_[15])
        decoded_payload['TEMP'] = floor(bin16dec(temp) / 10)

    else:
        print('ERROR: PCR2 application payload should start with be0103..')

    return decoded_payload


def decode_v3_config_payload(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'Firmware': '', 'OperationMode': '', 'PayloadType': '',
                       'DeviceClass': '',
                       'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '', 'CapacityLimit': '',
                       'HoldoffTime': '',
                       'RadarSensitivity': ''}

    if port != 190:
        print('ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 20:
        print('ERROR: Wrong payload length')
        return None

    if bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x03:

        decoded_payload['DeviceType'] = bytes_[3]
        decoded_payload['Firmware'] = str(bytes_[4]) + '.' + str(bytes_[5]) + '.' + str(bytes_[6])
        decoded_payload['OperationMode'] = bytes_[7]
        decoded_payload['PayloadType'] = bytes_[8]
        decoded_payload['DeviceClass'] = bytes_[9]
        decoded_payload['UplinkType'] = bytes_[10]
        decoded_payload['UplinkInterval'] = (bytes_[11] << 8 | bytes_[12])
        decoded_payload['LinkCheckInterval'] = (bytes_[13] << 8 | bytes_[14])
        decoded_payload['CapacityLimit'] = (bytes_[15] << 8 | bytes_[16])
        decoded_payload['HoldoffTime'] = (bytes_[17] << 8 | bytes_[18])
        decoded_payload['RadarSensitivity'] = bytes_[19]

    else:
        print('ERROR: PCR2 application payload should start with be0103..')

    return decoded_payload


' Decoder Version 4 '


def decode_extended_v4(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'LTR': '', 'RTL': '', 'LTR_SUM': '', 'RTL_SUM': '', 'SBX_BATT': '', 'SBX_PV': '', 'DIFF': '',
                       'TEMP': ''}

    if port != 14:
        print('ERROR: Wrong port! PCR2 devices are using port 14 for application payloads.')
        return None

    if len(bytes_) != 17:
        print('ERROR: Wrong payload length')
        return None

    if bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x04:

        decoded_payload['RTL'] = (bytes_[3] << 8 | bytes_[4])
        decoded_payload['LTR'] = (bytes_[5] << 8 | bytes_[6])
        decoded_payload['LTR_SUM'] = (bytes_[7] << 8 | bytes_[8])
        decoded_payload['RTL_SUM'] = (bytes_[9] << 8 | bytes_[10])
        decoded_payload['SBX_BATT'] = (bytes_[11] << 8 | bytes_[12])
        decoded_payload['SBX_PV'] = (bytes_[13] << 8 | bytes_[14])
        decoded_payload['DIFF'] = abs(decoded_payload['LTR_SUM'] - decoded_payload['RTL_SUM'])
        temp = (bytes_[15] << 8) | (bytes_[16])
        decoded_payload['TEMP'] = floor(bin16dec(temp) / 10)

    else:
        print('ERROR: PCR2 application payload should start with be0104..')

    return decoded_payload


def decode_v4_config_payload(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'Firmware': '', 'OperationMode': '', 'PayloadType': '',
                       'DeviceClass': '', 'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '',
                       'CapacityLimit': '',
                       'HoldoffTime': '', 'InactivityTimeout': '', 'MountingDirection': '',
                       'MountingTilt': '', 'DetectionAngle': '', 'MinDist': '', 'MaxDist': '', 'MinSpeed': '',
                       'MaxSpeed': '', 'RadarSensitivity': ''}

    if port != 190:
        print('ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 31:
        print('ERROR: Wrong payload length')
        return None

    if bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x04:

        decoded_payload['DeviceType'] = bytes_[3]
        decoded_payload['Firmware'] = str(bytes_[4]) + '.' + str(bytes_[5]) + '.' + str(bytes_[6])
        decoded_payload['OperationMode'] = bytes_[7]
        decoded_payload['PayloadType'] = bytes_[8]
        decoded_payload['DeviceClass'] = bytes_[9]
        decoded_payload['UplinkType'] = bytes_[10]
        decoded_payload['UplinkInterval'] = (bytes_[11] << 8 | bytes_[12])
        decoded_payload['LinkCheckInterval'] = (bytes_[13] << 8 | bytes_[14])
        decoded_payload['CapacityLimit'] = (bytes_[15] << 8 | bytes_[16])
        decoded_payload['HoldoffTime'] = (bytes_[17] << 8 | bytes_[18])
        decoded_payload['InactivityTimeout'] = (bytes_[19] << 8 | bytes_[20])
        decoded_payload['MountingDirection'] = bytes_[21]
        decoded_payload['MountingTilt'] = bytes_[22]
        decoded_payload['DetectionAngle'] = bytes_[23]
        decoded_payload['MinDist'] = (bytes_[24] << 8 | bytes_[25])
        decoded_payload['MaxDist'] = (bytes_[26] << 8 | bytes_[27])
        decoded_payload['MinSpeed'] = bytes_[28]
        decoded_payload['MaxSpeed'] = bytes_[29]
        decoded_payload['RadarSensitivity'] = bytes_[30]

    else:
        print('ERROR: PCR2 application payload should start with be0104..')

    return decoded_payload


' Decoder Version 5 '

def decode_v5_config_payload(payload_hex, port):
    bytes_ = bytearray.fromhex(payload_hex)
    decoded_payload = {'DeviceType': '', 'Firmware': '', 'OperationMode': '', 'PayloadType': '',
                       'DeviceClass': '', 'UplinkType': '', 'UplinkInterval': '', 'LinkCheckInterval': '',
                       'CapacityLimit': '',
                       'HoldoffTime': '', 'InactivityTimeout': '', 'RadarEnabled': '',
                       'BeamAngle': '', 'MinDist': '', 'MaxDist': '', 'MinSpeed': '',
                       'MaxSpeed': '', 'RadarAutotune': '', 'RadarSensitivity': '', 'SBXVersion': ''}

    if port != 190:
        print('ERROR: Wrong port! PCR2 devices are using port 190 for application payloads.')
        return None

    if len(bytes_) != 34:
        print('ERROR: Wrong payload length')
        return None

    if bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x05:

        decoded_payload['DeviceType'] = bytes_[3]
        decoded_payload['Firmware'] = str(bytes_[4]) + '.' + str(bytes_[5]) + '.' + str(bytes_[6])
        decoded_payload['OperationMode'] = bytes_[7]
        decoded_payload['PayloadType'] = bytes_[8]
        decoded_payload['DeviceClass'] = bytes_[9]
        decoded_payload['UplinkType'] = bytes_[10]
        decoded_payload['UplinkInterval'] = (bytes_[11] << 8 | bytes_[12])
        decoded_payload['LinkCheckInterval'] = (bytes_[13] << 8 | bytes_[14])
        decoded_payload['CapacityLimit'] = (bytes_[15] << 8 | bytes_[16])
        decoded_payload['HoldoffTime'] = (bytes_[17] << 8 | bytes_[18])
        decoded_payload['InactivityTimeout'] = (bytes_[19] << 8 | bytes_[20])
        decoded_payload['RadarEnabled'] = bytes_[21]
        decoded_payload['BeamAngle'] = bytes_[22]
        decoded_payload['MinDist'] = (bytes_[23] << 8 | bytes_[24])
        decoded_payload['MaxDist'] = (bytes_[25] << 8 | bytes_[26])
        decoded_payload['MinSpeed'] = bytes_[27]
        decoded_payload['MaxSpeed'] = bytes_[28]
        decoded_payload['RadarAutotune'] = bytes_[29]
        decoded_payload['RadarSensitivity'] = bytes_[30]
        decoded_payload['SBXVersion'] = str(bytes_[31]) + '.' + str(bytes_[32]) + '.' + str(bytes_[33])

    else:
        print('ERROR: PCR2 application payload V5 should start with be0105..')

    return decoded_payload



def bin16dec(bin_):
    num = bin_ & 0xFFFF
    if 0x8000 & num:
        num = -(0x010000 - num)
    return num


' Generic Decoder '


def decode(payload, port):
    bytes_ = bytearray.fromhex(payload)
    if port == 14:
        if bytes_[0] == 0x0a:
            return decode_elsys_pl(payload, port)
        elif bytes_[0] == 0 and bytes_[1] == 102:
            # decode lpp payload
            return None
        elif bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x03:
            return decode_extended_v3(payload, port)
        elif bytes_[0] == 0xbe and bytes_[1] == 0x01 and bytes_[2] == 0x04:
            return decode_extended_v4(payload, port)
        else:
            print('ERROR: No decoder for application payload')

    if port == 190:
        if len(bytes_) == 14:
            return decode_v2_config_payload(payload, port)
        elif len(bytes_) == 20:
            return decode_v3_config_payload(payload, port)
        elif len(bytes_) == 31:
            return decode_v4_config_payload(payload, port)
        elif len(bytes_) == 34:
            return decode_v5_config_payload(payload, port)    
        else:
            print('ERROR: No decoder for config payload')
